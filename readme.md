New Project Base Steps

- npm update -g npm
- npm install -g grunt-cli
- npm init (or create a file called package.json with an empty object {})

Project Dev Dependancies

- npm install grunt --save-dev
- npm install grunt-contrib-jshint --save-dev

- create a file called Gruntfile.js
- with the following

	module.exports = function(grunt) {

		// Project configuration.
		grunt.initConfig({
			jshint: {
				all: ['Gruntfile.js', 'app/**/*.js']
			}
		});

		// Load the plugin that provides the "jshint" task.
		grunt.loadNpmTasks('grunt-contrib-jshint');

		// Default task(s).
		grunt.registerTask('default', ['jshint']);

	};

- type grunt at the command line to verify

	$ grunt
	Running "jshint:all" (jshint) task
	>> 1 file lint free.

- Done.

------------------------
- npm install browserify --save-dev
- npm install grunt-browserify --save-dev

- npm install angular --save
- npm install lodash --save

Notes
- http://browserify.org/
- https://webpack.github.io/docs/commonjs.html
- (did not follow everything here, just the basic idea) https://blog.codecentric.de/en/2014/08/angularjs-browserify/
- https://github.com/buildfirst/buildfirst/tree/master/ch05