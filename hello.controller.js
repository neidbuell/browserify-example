(function() {
    'strict mode';
    var angular = require('angular');
    var _ = require('lodash');

    angular.module('app')
        .controller('Hello', Hello);

    Hello.$inject = [];

    function Hello () {
        var vm = this;
        vm.salutation = _.upperFirst('hello! ') +  _.upperFirst('have a great day!');
    }
})();