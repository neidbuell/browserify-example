module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        jshint: {
            all: ['Gruntfile.js', '*.js', '!bundle.js']
        },
        browserify: {
            options: {
                debug: true
            },
            debug: {
                files: {
                    'bundle.js': ['app.module.js', 'hello.controller.js']
                }
            }
        }
    });

    // Load the plugin that provides the "jshint" task.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('browserify');
    grunt.loadNpmTasks('grunt-browserify');

    // Default task(s).
    grunt.registerTask('default', ['jshint', 'browserify']);

};